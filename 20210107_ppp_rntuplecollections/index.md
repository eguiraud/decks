---
title: Accessing column sizes in RDF+RNTuple
author: Enrico & Jakob
date: PPP meeting, 7/1/2021
---

```c++
df.Fiter("jets.size() > 0")
```

- `jets` is unnecessarily deserialized
- `RNTuple` has facilities to access size separately from contents
- `RNTupleDS` would benefit from a syntax to indicate "size of..."

---

```c++
df.Fiter("#jets > 0")
```

- need to teach the tokenizer that tokens can start with `#`
- breaks "column names are valid C++ variable names", in a controlled manner

```c++
df.Fiter("__jets_sz > 0")
```

Potential name collisions

---

- savings would be noticeable in case of collections of complex objects
- consensus on one of the options? other ideas?

<link rel="stylesheet" href="styles.css">
