---
title: "RDF: plan of work 2022"
author: Enrico Guiraud
date: ROOT meeting, 17/01/2022
slideNumber: 1
---

## RDataFrame PoW Items completed in 2021

- Investigate design for categories of data (`df.DefinePerSample`)
- Convenient parameter variation ("systematics") within the same event loop (`df.Vary`, ongoing)
- Column redefinition (`df.Redefine`)
- Fix RDF false sharing
- Debug/verbose mode

## Important items that we failed to tackle in 2021

- Bulk processing of data in RDF's inner loop (at least for RNTuple data)
- Make nested parallelism safe ([ROOT-10269](https://sft.its.cern.ch/jira/browse/ROOT-10269))
- TMVA+RDF: fast inference with SOFIE + RDF adapter
- Improve Python API of a typical RDF analysis: less C++ code strings / helper functions; performant python->RDF calls

## 2022: must have (1/2)

- Make nested parallelism safe ([ROOT-10269](https://sft.its.cern.ch/jira/browse/ROOT-10269))
- TMVA+RDF: Finalize fast inference from ONNX format for DNN+CNN, including RDF adapter ([#9242](https://github.com//issues/9242))
- Bulk processing of data in RDF's inner loop (at least for RNTuple data)

## 2022: must have (2/2)

- RNTuple+RDF integration: fix RVec usage (finalize [#8770](https://github.com//pull/8770), fix [#6347](https://github.com//issues/6347))
- Reduce the need to use C++ expressions in strings (e.g. directly accept Numba-jittable Python functions in Define/Filter, see [this talk](https://eguiraud.web.cern.ch/eguiraud/decks/20211014_ppp_new_rdf_interfaces/))
- Better support for partial column availability ([#8704](https://github.com//issues/8704))

## 2022: should have

- Operations on groups of array columns, e.g. simultaneous selection of elements (see [this talk](https://eguiraud.web.cern.ch/eguiraud/decks/20211014_ppp_new_rdf_interfaces/), [this forum thread](https://root-forum.cern.ch/t/is-there-better-way-to-filter-array-branches-than-defining-new-columns-in-rdataframe/45495))
- Add a constructor that takes a dataset specification object (see [this talk](https://eguiraud.web.cern.ch/eguiraud/decks/20211014_ppp_new_rdf_interfaces)). Useful for:
   - adding custom labels to different samples for `DefinePerSample`
   - specifying entry ranges without `TEntryList`

## 2022: nice to have

- RNTuple+RDF: investigate and address remaining performance bottlenecks
- AsNumpy should automatically convert RVecs into NumPy arrays ([#7685](https://github.com//issues/7685))
- TMVA+RDF: Provide a generator of batches of events useful for injecting ROOT data into third-party ML training frameworks
- Add a progress bar to RDF ([#8685](https://github.com//issues/8685))
- Add a `GraphAsymmErrors` method ([#8363](https://github.com//issues/8363))
- Add `DefineMany` ([ROOT-9766](https://sft.its.cern.ch/jira/browse/ROOT-9766))

## EOF

<link rel="stylesheet" href="styles.css">
