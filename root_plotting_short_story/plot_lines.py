import matplotlib.pyplot as plt

x = [5,10,20,35,50,75,100]
rntupleds_before = [341125,673832,1413541,2510093,3538869,5436099,7385743]
rntupleds_after = [212397,442021,843143,1445159,2035215,3419880,4623714]
rntuple = [90327,172209,337874,576299,815966,1519551,2029339]

plt.plot(x, rntupleds_before, markersize=5, marker='o', label="RNTupleDS before")
plt.plot(x, rntuple, markersize=5, marker='o', label="RNTuple")

plt.ylim(0, plt.ylim()[1])
plt.xlim(0, plt.xlim()[1])
plt.xlabel("% of events processed")
plt.ylabel("runtime (us)")
plt.title("RNTuple LHCB benchmark")
plt.tight_layout()

plt.legend()
plt.show()
