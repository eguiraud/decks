---
title: "Plots with ROOT: a short story"
author: Enrico Guiraud
date: October 1 2020
---

<link rel="stylesheet" href="styles.css">

![](root.svg)

This took me ~1 hour with ROOT.

---

```python
import ROOT

xs = [5,10,20,35,50,75,100]

rntupleds_before = [341125,673832,1413541,2510093,3538869,5436099,7385743]
rntuple = [90327,172209,337874,576299,815966,1519551,2029339]
data = [rntupleds_before, rntuple]

gbefore = ROOT.TGraph()
gntuple = ROOT.TGraph()
graphs = [gbefore, gntuple]

for d, graph in zip(data, graphs):
    for x, y in zip(xs, d):
        graph.SetPoint(graph.GetN(), x, y)

gbefore.SetTitle("RNTupleDS")
gntuple.SetTitle("RNTuple")
for g in graphs:
    g.SetMarkerStyle(20)
    g.SetLineWidth(3)
    g.SetMarkerSize(1)

mg = ROOT.TMultiGraph()
for g in graphs:
    mg.Add(g)

mg.SetTitle("RNTuple LHCB benchmark")
mg.Draw("ACPPLCPMC")
xax = mg.GetXaxis()
xax.SetRangeUser(0, 100)
xax.SetTitle("% of events processed")
yax = mg.GetYaxis()
yax.SetRangeUser(0, yax.GetXmax())
yax.SetTitle("runtime (us)")

ROOT.gPad.BuildLegend()
```

---

<img src=root.svg height=200px>

* yellow as second color is a bad default, yellow on white is hard to read
* font size inside the legend is weirdly large, larger than the title
* automatic legend placement puts the legend on top of ticks and on part of the line

---

* magical incantations gone wrong:

```python

# this produces a blank canvas with no indication as to why
mg.Draw("CPPLCPMC")

# "you didn't tell it to draw the axis" (...but isn't it kind of implied?)
mg.Draw("ACPPLCPMC")
```

* also there is no way to draw 3 TGraphs and a legend and set a title for the whole plot: you need to register the TGraphs with a TMultiGraph, because both the first legend entry and the plot title default to the title of the first object drawn and I could find no way to override the plot title

---

![](matplotlib.svg)

This took me ~3 minutes with matplotlib.

---

```python
import matplotlib.pyplot as plt

x = [5,10,20,35,50,75,100]
rntupleds_before = [341125,673832,1413541,2510093,3538869,5436099,7385743]
rntuple = [90327,172209,337874,576299,815966,1519551,2029339]

plt.plot(x, rntupleds_before, markersize=5, marker='o', label="RNTupleDS before")
plt.plot(x, rntuple, markersize=5, marker='o', label="RNTuple")

plt.ylim(0, plt.ylim()[1])
plt.xlim(0, plt.xlim()[1])
plt.xlabel("% of events processed")
plt.ylabel("runtime (us)")
plt.title("RNTuple LHCB benchmark")
plt.tight_layout()

plt.legend()
plt.show()
```
