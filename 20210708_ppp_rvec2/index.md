---
title: RVec 2.0
author: Enrico Guiraud
date: July 8 2021, 109th PPP meeting
---

## RVec 1.0: pros and cons

---

### std::vector on steroids

- API compatible with std::vector
- powered by a [custom allocator](https://github.com/root-project/root/blob/master/math/vecops/inc/ROOT/RAdoptAllocator.hxx)
- memory adoption via a dedicated ctor:<br>`RVec(ptr, size)`
- numpy-like math ops: `sqrt(v1*v1 + v2*v2)`
- simple masking: `return v1[v2 + v3 > 0]`

<link rel="stylesheet" href="styles.css">

---

### Many small allocs in the event loop

```cpp
return v1[v2 + v3 > 0]
```

That's 3 allocations and 3 deallocations.

9% of runtime in the [ZPeak integration benchmark](https://github.com/root-project/rootbench/blob/master/root/tree/dataframe/zpeak.cxx).

---

### I/O is tricky

- due to the presence of the custom allocator, `RVec`s have a different memory layout than `std::vector`s
- due to the implementation in terms of `std::vector`, `RVec<bool>` is "special"
- `RNTuple` is [off by one byte](https://github.com/root-project/root/issues/6347) when deserializing into RVecs

---

### Consensus from [PPP 85](https://indico.cern.ch/e/PPP85)

Redesign based on LLVM's SmallVector to add small buffer optimization, provided:

- we pick the right size
- works well with RNTuple I/O
- the small buffer size is tweakable by expert users

---

## RVec 2.0

---

### In a nutshell

```cpp
template <typename T>
class RVec {
   std::vector<T, RAdoptAllocator<T>>;
   ...
};
```

becomes

```cpp
template <typename T>
class RVec {
   void *fBegin;
   int fSize;
   int fCapacity; // -1 means non-owning
   alignas(T) char buf[N*sizeof(T)];
   ...
};
```

---

### Actual class hierarchy

<div style="font-size: 0.7em">
```cpp
class SmallVectorBase {
   void *fBegin;
   int fSize;
   int fCapacity;
};

template <typename T>
class SmallVectorTemplateCommon : public SmallVectorBase;

template <typename T, bool = IsPOD<T>>
class SmallVectorTemplateBase
   : public SmallVectorTemplateCommon<T>;

template <typename T>
class SmallVectorTemplateBase<T, true>
   : public SmallVectorTemplateCommon<T>;

template <typename T>
class RVecImpl : public SmallVectorTemplateBase<T>;

template <typename T, unsigned N>
struct SmallVectorStorage {
   alignas(T) char buf[N*sizeof(T)];
};

template <typename T, unsigned N>
class RVecN : public RVecImpl<T>, SmallVectorStorage<T, N>;

template <typename T>
class RVec : public RVecN<T, RVecInlineStorageSize<T>::value>;
```
</div>

---

### The ARCHITECTURE.md file

A design document to help developers that want to familiarize with the code.
[Current version of the doc](https://github.com/eguiraud/root/blob/820043343db016085110da8998b8c6babe103a05/math/vecops/ARCHITECTURE.md).
More motivation [here](https://matklad.github.io/2021/02/06/ARCHITECTURE.md.html).

---

### Current policy for the small buffer size

"As many elements as possible as long as one RVec fits in a cache line, or at least 8, unless that would occupy more than 1 kB".

Code is [here](https://github.com/eguiraud/root/blob/820043343db016085110da8998b8c6babe103a05/math/vecops/inc/ROOT/RVec.hxx#L530-L545).

---

### Exception safety

- LLVM's SmallVector "does not attempt to be exception safe"
- we understand this as "bad things might happen if element ctor can throw"
- otoh, we expect majority of RVec usage with fundamental types
- ...and also expect exceptions thrown during event loop to be non-recoverable/a bug in the analysis code

So we did not attempt to fix this for now, can revisit if needed.

---

### RVec 2.0: I/O

Now backed by collection proxies, but change is not fully backward compatible.

Version   |   RDF behavior                 |   TTree/TFile behavior
-------   |   ------------                 |   --------------------
6.22      |   Write RVecs as std::vectors  |   users can write RVecs to TTrees, won't be able to read them back w/ TTreeReader or RDF
6.24      |   Write RVecs as std::vectors  |   if users write RVecs to TTrees, warn they won't be able to read them back in 6.26
6.26      |   Write RVecs as RVecs, with collection proxies | I/O of RVecs just works. Error printed when reading RVecs 1.0

---

## Micro-benchmarks

---

###

<object data="before.svg" type="image/svg+xml" style="height:14em"></object>

---

### What's taking so long?

[let's check with perf]

---

###

<object data="after.svg" type="image/svg+xml" style="height:14em"></object>

---

###

SBO buys us a lot for small arrays but it has a costs for larger arrays.

What's the impact on actual applications?

---

## Integration benchmarks

Averages over three runs on ntpl-perf01. Measuring wall-clock time.

Benchmarks at [root-project/rootbench](https://github.com/root-project/rootbench).

---

                                      Runtime % change
-----------                           ---------------
**Wmass ST**                          +0.58%
**NanoAODDimuonAnalysis.cpp**         no change
**Wmass MT**                          -1.00%
**LoopSUSYFrame**                     -1.83%
**ZPeak ST**                          -2.16%
**NanoAODDimuonAnalysis.py MT**       -2.85%
**NanoAODDimuonAnalysis.py ST**       -4.66%
**ZPeak MT**                          -8.14%
**NanoAODHiggsAnalysis.cpp ST**       -16.52%
**NanoAODHiggsAnalysis.py MT**        -17.57%
**NanoAODHiggsAnalysis.cpp MT**       -18.28%
**NanoAODHiggsAnalysis.py ST**        -20.44%
-----------------------------------------------------

---

## Summary

New RVec ready for merging. Open threads:

- picking the right size: could be fine-tuned guided by integration benchmarks
- works well with RNTuple I/O: no problems foreseen with the current design
- tweakable small buffer size: we expose RVecN, to be figured out how to implement all goodies in term of it in a backward-compatible way

---

## Backslides

---

### Muon sizes

<object data="muon_sizes.svg" type="image/svg+xml" style="max-inline-size:100%"></object>

---

### GenPart sizes

<object data="el_sizes.svg" type="image/svg+xml" style="max-inline-size:100%"></object>

---

### Tau sizes

<object data="tau_sizes.svg" type="image/svg+xml" style="max-inline-size:100%"></object>
