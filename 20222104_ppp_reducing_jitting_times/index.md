---
title: Reducing jitting times in RDF and ROOT
author: Enrico Guiraud
date: PPP meeting, 21/04/2022
slideNumber: 1
---

## RDF jitting time is pure overhead

It is done right before the RDF event loop to "fill gaps" in the computation graph that were left at compile-time.

It is a single-thread start-up overhead.

---

### RDF jitting in a nutshell

How and why Filters and Defines are jitted:

```cpp
df.Filter(myFunc, {"x"}); // -> RFilter node, no jitting
df.Filter("x > 0"); // -> RJittedFilter node (an "empty shell"), requires jitting
```

```cpp
// during jitting (retrieving column types at runtime)
auto lambda = [](double x) { return x > 0; };
jittedFilter->SetConcreteFilter(RFilter(lambda, {"x"}));
```

---

### RDF jitting in a nutshell

Similarly for actions:

```cpp
df.Snapshot("t", "f.root"); // -> RJittedAction
```

```cpp
// during jitting (retrieving column types at runtime)
jittedAction->SetConcreteAction(
   RSnapshot<double, float, RVecD>("t", "f.root", {"x", "y", "v"}));
```

. . .

Less typing, simpler API, at the cost of introducing virtual calls.

. . .

Jitting times are _usually_ O(0.1) to O(1) seconds.

---

## Jitting times are compile times (at runtime)

Most often, what makes jitting slow is the same as what makes compile times slow.
For RDF usually:

- template instantiations
- _recursive_ template instantiations (looking at you, `std::tuple`)

. . .

Compiling the equivalent of the code that gets jitted with g++ or clang++ typically
gives consistent results (i.e. cling is _usually_ not the bottleneck).

---

## The rule of Chiel on compile-time costs

<img src=./rule_of_chiel.png style="height:300px">

From [Type Based Template Metapgrogramming is Not Dead, Odin Holmes](https://www.youtube.com/watch?v=EtU4RDCCsiU&t=24s).

---

## However, generated code is special

- we more easily end up with _long_ template parameter lists (O(100) sometimes)
- we more easily end up with _large_ translation units and function bodies (thousands of lines for large computation graphs)

Most of the following slides show techniques we applied to make sure this does not happen.

---

## How RDF jitted code looks like

```cpp
// passed to gInterpreter->Declare
namespace R_rdf {
auto func1 = [](const double var0) { return var0 > 0; };
using func1_ret_t = CallableTraits<decltype(func1)>::ret_type;
auto func2 = [](RVecD &var0, RVecD &var1) { return var0[var1 > 0]; };
using func2_ret_t = CallableTraits<decltype(func2)>::ret_type;
// ...
}

// passed to gInterpreter->Calc
void ToRun() {
   JitFilterHelper(R_rdf::func1, (RDataFrame*)(0x123), {"x"});
   JitDefineHelper(R_rdf::func2, (RDataFrame*)(0x123), {"v1", "v2"});
   // ...
}
```

---

## Exception handling can be expensive

<object data="./flamegraph_exception_handling.svg" type="image/svg+xml" style="height:12em"></object>

---

## Avoid generation of exception handling code

```diff
- JitFilterHelper(..., std::vectorstd::string{"col1", "col2"})
+ JitFilterHelper(..., const char*[]{"col1", "col2"})
```

Done in [#7651](https://github.com/root-project/root/pull/7651), more discusssion at [#7604](https://github.com/root-project/root/issues/7604).

---

## Avoid generation of exception handling code

<img src=./vector_to_carray.png style="height:400px">

---

## Type erasure to avoid large std::tuples

```diff
template <typename...ColTypes>
class RAction {
-  std::tuple<unique_ptr<RColumnReader<ColTypes>>...> fInputCols;
+  array<unique_ptr<RColumnReaderBase>, sizeof...(ColTypes)> fInputCols;
};
```

Trades off compile time for runtime. Worth it to avoid extreme cases.
To get an idea of the gains, see [ROOT-9468](https://sft.its.cern.ch/jira/browse/ROOT-9468).

---

## Reusing lambdas

```diff
- auto lambda1 = [](const float x) { return x > 0; };
- auto lambda2 = [](const float y) { return y > 0; };
- ...
- JitFilterHelper(lambda1, {"x"});
- JitFilterHelper(lambda2, {"y"});
+ auto lambda1 = [](const float var0) { return var0 > 0; };
+ ...
+ JitFilterHelper(lambda1, {"x"});
+ JitFilterHelper(lambda1, {"y"});
```

Leverages the fact that analyses often perform the same computations on different columns.

Gain mostly from reusing memoized template instantiations.

Done in [#5329](https://github.com/root-project/root/pull/5329).
See [this PPP talk](https://indico.cern.ch/event/909884/#3-rdf-performance-measurements) for perf gains.

---

## Grouping multiple cling invocations together

Each call to `gInterpreter` has a certain overhead. It is faster to make one call with all code rather than many separate calls.

RDF jits and executes lazily, right before the event loop, all code necessary.

Done in [#5356](https://github.com/root-project/root/pull/5356).

---

## Splitting large function bodies

```patch
- void ToRun() {
-    JitFilterHelper(R_rdf::func1, (RDataFrame*)(0x123), {"x", "y"});
-    // 1999 more lines like this one
- }
+ void ToRun1() {
+    JitFilterHelper(R_rdf::func1, (RDataFrame*)(0x123), {"x", "y"});
+    // 999 more lines
+ }
+
+ void ToRun2() {
+    // the remaining 1000 lines
+ }
```

Especially with optimizations enabled, compiling a large function body can take much longer than compiling several smaller bodies. Done in [#9328](https://github.com/root-project/root/pull/9328).

---

## Finding the Goldilocks zone

Splitting large function bodies and grouping cling invocations together are at odds,
but the former is a more important optimization than the latter.

We converged to grouping everything at first and then do one invocation every 1k lines of code, some gains might be possible by fine-tuning this threshold.

---

## Optimizing compiler optimizations

- turning on O1 optimization impacted jitting times significantly
- Jonas H. found out that the register allocation optimization pass took a long time for no visible performance benefit
- it is now disabled in cling ([#9342](https://github.com/root-project/root/pull/9342))

---

## The benchmark

[This benchmark](https://rootbnch-grafana-test.cern.ch/d/G2qrd2SWk/rdataframe?orgId=1&viewPanel=42&from=1637041702175&to=1649668546249)
tracked most of the history.

<img src=./jit_large_graphs_1000.png style="height:400px">

---

## The benchmark

[This benchmark](https://rootbnch-grafana-test.cern.ch/d/G2qrd2SWk/rdataframe?orgId=1&viewPanel=42&from=1637041702175&to=1649668546249)
tracked most of the history.

<img src=./jit_large_graphs_10k.png style="height:400px">

---

## A cling-specific issue

Sometimes we still end up with deeply nested template arguments.

Cling is particularly slow here, whereas clang and gcc fare better, see [root-project/cling/issues/443](https://github.com/root-project/cling/issues/443).

---

## cling's nullptr checks

<img src=./nullptr_checks.svg style="height:400px">

This affects _runtime_ of jitted code, and it bit us a few times.

---

## cling's nullptr checks

Nullptr checks are now only enabled in the interactive ROOT prompt ([#9531](https://github.com/root-project/root/pull/9531), [#9548](https://github.com/root-project/root/pull/9548)).

Even then, we must turn off nullptr checks in our template classes ([#10228](https://github.com/root-project/root/pull/10228), [#9461](https://github.com/root-project/root/pull/9461), [#7004](https://github.com/root-project/root/pull/7004)):

```diff
- class RAction : public RActionBase {
+ class R__CLING_PTRCHECK(off) RAction : public RActionBase {
```

---

## Bonus: adding features increases runtimes

<img src=./syst_variations_jitting_times.png style="height:300px">

The systematic variations code is not invoked in this benchmark, but part of it has to be compiled!

## EOF

<link rel="stylesheet" href="styles.css">
