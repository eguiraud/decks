---
title: "RDF design questions"
author: Enrico Guiraud
date: PPP meeting, 02/06/2022
slideNumber: 1
---

## Auto-naming of varied histograms

---

### Currently

```python
df.Histo1D("pt")
```

Name/title: "pt"

```python
df.Histo1D(("myHist", "My hist title", 100, 0., 1.), "pt")
```

Name: "myHist"<br>
Title: "My hist title"

---

### What about variations?

```python
df.Vary("pt", ..., ["down", "up"]).Histo1D("pt")
```

Names/titles?<br>
E.g. `["pt", "pt_pt:down", "pt_pt:up"]`

```python
df.Vary("pt", ..., ["down", "up"]).Histo1D(("myHist", "My hist title", 100, 0., 1.), "pt")
```

Names/titles?

---

### Further questions

- should we let users customize this behavior at the level of RDF? or is it ok to fine-tune histogram names as a post-processing step?
- [it was mentioned](https://root-forum.cern.ch/t/rdataframe-systematics-variation-vary/50176/3?u=eguiraud) that some stat tools like Higgs Combine might not like the `:`, is this a problem?

---

## DefinePerSample, sample labels and friend trees

---

### Currently

<object data="chain.svg" type="image/svg+xml" style="height:11em"></object>

```python
df.DefinePerSample("w", 'rdfsampleinfo_.Contains("tree1") ? 0.5 : 1.')
```

---

### With labels

<object data="chain_labels.svg" type="image/svg+xml" style="height:11em"></object>

```python
df.DefinePerSample("w", 'rdfsampleinfo_.Label() == "MC" ? 0.5 : 1.')
```

---

### With labels and friends?

<object data="chain_labels_friends.svg" type="image/svg+xml" style="height:11em"></object>

This is really a generic problem with DefinePerSample and friends, labels only make things uglier.

---

### Questions

- is there a need for accessing information about friends from `rdfsampleinfo_`?
- should it be possible to label each sample in a friend?

## EOF

<link rel="stylesheet" href="styles.css">
