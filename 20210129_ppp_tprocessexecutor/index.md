---
title: The TProcessExecutor incident
author: Jonas & Enrico
date: PPP meeting, 3/2/2021
---

**The symptom**<br>

- failure in `tExecutor` test on MacOS, Linux
- failure in `tProcessExecutor` test on MacOS

**The cause**<br>

Deadlock at teardown in tests related to `TProcessExecutor`.
`fork()`-ing a multi-thread program without ensuring sane state of the locks (actually impossible if we don't control the threads!).

---

**Who spawns threads in ROOT applications?**<br>

- Cocoa on MacOS (causes `tProcessExecutor` failure on MacOS)
- TBB when `RTaskArena` is created (causes `tExecutor` failure on MacOS, Linux)
- potentially, users

---

**What can we do about this?**<br>

- tests fixed by ensuring we only fork single-thread programs
- can we detect this scenario and warn at runtime?
- should we always warn on Mac when TProcessExecutor is used and `!gROOT->IsBatch()`?

<script>
document.getElementsByTagName("head")[0].insertAdjacentHTML(
    "beforeend",
    "<link rel=\"stylesheet\" href=\"styles.css\" />");
</script>


