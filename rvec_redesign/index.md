---
title: RVec redesign and small buffer optimization
author: Enrico Guiraud
date: November 10 2020, 85th PPP meeting
---

## Current RVec: pros and cons

### std::vector on steroids

Memory adoption via a dedicated ctor:<br>
`RVec(ptr, size)`

. . .

(powered by a custom allocator)

. . .

Friendlier syntax:<br>
`auto v3 = sqrt(v1*v1 + v2*v2)`
<link rel="stylesheet" href="styles.css">

### Many small allocations/deallocations

`return v1[v2 + v3 > 0]`

That's 4 allocations and 4 deallocations.

9% of runtime in the [ZPeak integration benchmark](https://github.com/root-project/rootbench/blob/master/root/tree/dataframe/zpeak.cxx).

### I/O is tricky

::: incremental

- due to the presence of the custom allocator, `RVec`s have a different memory layout than `std::vector`s
  - `RNTuple` is [off by one byte](https://github.com/root-project/root/issues/6347) when deserializing into RVecs
- `TTreeReaderArray<T>` cannot read `RVec<T>` (would need a custom `TCollectionProxy` implementation)

:::

## Redesign proposal

### Rewriting RVec internals

::: incremental

- introduce a small buffer optimization (SBO) to reduce allocations
- handle allocations manually (ditch `RAdoptAllocator`):
  - simpler to implement SBO and memory adoption
  - memory layout does not depend on `std::vector`'s impl. details
  - deserialization logic (e.g. in `RNTuple`) becomes relatively simple to write
- no change required to all operators defined for the `RVec` type

:::

### Design based on LLVM's SmallVector

[peeking into llvm::SmallVector with sourcetrail]

### In a nutshell

```cpp
template <typename T>
class RVec {
   std::vector<T, RAdoptAllocator<T>>;
   ...
};
```

becomes

```cpp
template <typename T>
class RVec {
   T *fBegin;
   Size_t fSize;
   Size_t fCapacity;
   bool fOwns;
   alignas(T) char buf[N*sizeof(T)];
   ...
};
```

## Benchmarks

### Micro-benchmarks (1/3)

 Bench (times in ns)    `RVec`            `SmallVec<8>`       RVec2
 --------------------- --------------- --------------------  -------
 Assign/4                 4.78                 3.06           3.06
 Assign/8                 4.84                 2.51           2.56
 Assign/16                4.73                 2.51           2.56
 Assign/64                6.15                 5.01           3.66
 Add/4                    22.0                 6.13           6.16
 Add/8                    22.4                 6.41           6.46
 Add/16                   23.4                 22.8           23.6
 Add/64                   31.8                 30.0           29.6

### Micro-benchmarks (2/3)

Bench (times in ns)     `RVec`       `SmallVec<8>`       RVec2
---------------------- ---------- --------------------  -------
InvariantMasses/4         101            98.5             90.6
InvariantMasses/8         196             192              186
InvariantMasses/16        372             397              376
InvariantMasses/64       1428            1533             1436
MaskingComplex/4          155            48.4             46.6
MaskingComplex/8          168            63.4             63.6
MaskingComplex/16         202             214              216
MaskingComplex/64         425             441              436

### Micro-benchmarks (3/3)

SBO buys us a lot for small arrays and it costs little for large arrays (rare occurrence).

. . .

Completely standalone bench repo available at [eguiraud/vectober](https://github.com/eguiraud/vectober).<br>
Many thanks to Stefan for writing the benchmarks.

### Integration benchmarks

Warm cache, on my workstation (4x i7-4790 CPU @ 3.60GHz), gcc 8.3.0.
Measuring wall-clock time.

Benchmarks at [root-project/rootbench](https://github.com/root-project/rootbench).

### Integration benchmarks

-----------                           ---------------
**W mass**                            7% slower
**LoopSUSYFrame**                     2% faster
**Zpeak ST**                          8% faster
**Zpeak HT**                          7% faster
**df102 NanoAOD Dimuon (C++)**        3% faster
**df102 NanoAOD Dimuon (Python)**     11.3% faster
**df103 NanoAOD Higgs (C++)**         17.5% faster
**df103 NanoAOD Higgs (Python)**      23% faster
-----------------------------------------------------

## Open questions

### How should Snapshot write RVec2 to disk?

Currently `RVec` written as `std::vector<T, RAdoptAllocator>`, which won't be an option.

It's a change in behavior -- a breaking one, if the new impl. can't be read back as `std::vector`.

### Do we still need memory adoption?

::: incremental
- original motivation was to avoid copies from `TTreeReaderArray`, but a copy into a stack-allocated buffer is cheap
- would simplify RVec2's logic
- would simplify RDataFrame's logic: no need to distinguish between contiguous and non-contiguous `TTreeReaderArray` contents
:::

. . .

But `PyROOT`'s `AsRVec` would not be zero-copy anymore, which is a behavior we advertise.

### What should the size of the small buffer be?

Let's look at typical array sizes in CMS open data (NanoAOD-style).

### Muon sizes

<object data="muon_sizes.svg" type="image/svg+xml" style="max-inline-size:100%"></object>

### GenPart sizes

<object data="el_sizes.svg" type="image/svg+xml" style="max-inline-size:100%"></object>

### Tau sizes

<object data="tau_sizes.svg" type="image/svg+xml" style="max-inline-size:100%"></object>
