---
title: The `R__ASSERT` problem
author: Enrico Guiraud
date: July 19 2021, ROOT team meeting
---

### `R__ASSERT`

Like `assert`, but:

- makes use of ROOT's logging infrastructure
- guarantees graceful termination (e.g. closing ROOT files)
- it's not turned off in production

### Assertions on program invariants

- serve as documentation
- ensure invariants are kept despite code modifications
- might be expensive and are not meant to be user-facing
- typically turned off in production

```cpp
// So far we should get here only if we encounter a split collection of a class that contains
// directly a collection.
R__ASSERT(numberOfVarDim==1 && maininfo);
```

### Input validation

- helpful for users
- should provide a user-friendly error message
- should not be turned off in production

```cpp
Int_t TH1Merger::CheckForDuplicateLabels(const TH1 * hist) {
   R__ASSERT(hist != nullptr);
```

### The problem

`R__ASSERT` has historically been used for both purposes, but:

- given its name, some devs might think it is compiled away with `-DNDEBUG`, like `assert` (it just happened to me and Jonas H.)
- it cannot be used to assert on invariants in performance-critical code sections

### What do we do?

1. do we want a version of `R__ASSERT` that honors `-DNDEBUG`? should we just use `assert` for that purpose?
2. do we want to leave `R__ASSERT` as it is, or should we move to a different name?

### Further discussion
- on the ambiguity in usage [here](https://github.com/root-project/root/pull/8587)
- on using `R__ASSERT` specifically in headers [here](https://mattermost.web.cern.ch/root/pl/shojmngpz7fsby8pqkhtdcocew)

