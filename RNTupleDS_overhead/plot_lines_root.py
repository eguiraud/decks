import ROOT

xs = [5,10,20,35,50,75,100]

rntupleds_before = [341125,673832,1413541,2510093,3538869,5436099,7385743]
rntupleds_after = [212397,442021,843143,1445159,2035215,3419880,4623714]
rntuple = [90327,172209,337874,576299,815966,1519551,2029339]
#data = [rntupleds_before, rntupleds_after, rntuple]
data = [rntupleds_before, rntuple]

gbefore = ROOT.TGraph()
gafter = ROOT.TGraph()
gntuple = ROOT.TGraph()
#graphs = [gbefore, gafter, gntuple]
graphs = [gbefore, gntuple]

for d, graph in zip(data, graphs):
    for x, y in zip(xs, d):
        graph.SetPoint(graph.GetN(), x, y)

gbefore.SetTitle("RNTupleDS")
gafter.SetTitle("RNTupleDS after")
gntuple.SetTitle("RNTuple")
for g in graphs:
    g.SetMarkerStyle(20)
    g.SetLineWidth(3)
    g.SetMarkerSize(1)

mg = ROOT.TMultiGraph()
for g in graphs:
    mg.Add(g)

mg.SetTitle("RNTuple LHCB benchmark")
mg.Draw("ACPPLCPMC")
xax = mg.GetXaxis()
xax.SetRangeUser(0, 100)
xax.SetTitle("% of events processed")
yax = mg.GetYaxis()
yax.SetRangeUser(0, yax.GetXmax())
yax.SetTitle("runtime (us)")

ROOT.gPad.BuildLegend()
