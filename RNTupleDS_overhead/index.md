---
title: "RDF+RNTuple: better integration"
author: Enrico Guiraud + Jakob Blomer
date: September 23 2020
---

## The benchmark

### A "fruit fly" analysis

uncompressed, warm cache

scalars only, 1 histogram

single thread execution, SSD

|
-----|-----
#entries|42.8M
tot file size|7514 MiB
of which read|6624 MiB
#fields|26
of which read|18

---

In other words, a scenario where we expect RDF's overhead to be particularly visible.

### The full analysis (RDF version)

```cpp
auto fn_muon_cut = [](int is_muon) { return !is_muon; };
auto fn_k_cut = [](double prob_k) { return prob_k > 0.5; };
auto fn_pi_cut = [](double prob_pi) { return prob_pi < 0.5; };
auto fn_sum = [](double p1, double p2, double p3) { return p1 + p2 + p3; };
auto fn_mass = [](double B_E, double B_P2) { return sqrt(B_E*B_E - B_P2); };

auto df_muon_cut = frame.Filter(fn_muon_cut, {"H1_isMuon"})
                        .Filter(fn_muon_cut, {"H2_isMuon"})
                        .Filter(fn_muon_cut, {"H3_isMuon"});
auto df_k_cut = df_muon_cut.Filter(fn_k_cut, {"H1_ProbK"})
                           .Filter(fn_k_cut, {"H2_ProbK"})
                           .Filter(fn_k_cut, {"H3_ProbK"});
auto df_pi_cut = df_k_cut.Filter(fn_pi_cut, {"H1_ProbPi"})
                         .Filter(fn_pi_cut, {"H2_ProbPi"})
                         .Filter(fn_pi_cut, {"H3_ProbPi"});
auto df_mass = df_pi_cut.Define("B_PX", fn_sum, {"H1_PX", "H2_PX", "H3_PX"})
                        .Define("B_PY", fn_sum, {"H1_PY", "H2_PY", "H3_PY"})
                        .Define("B_PZ", fn_sum, {"H1_PZ", "H2_PZ", "H3_PZ"})
                        .Define("B_P2", GetP2, {"B_PX", "B_PY", "B_PZ"})
                        .Define("K1_E", GetKE, {"H1_PX", "H1_PY", "H1_PZ"})
                        .Define("K2_E", GetKE, {"H2_PX", "H2_PY", "H2_PZ"})
                        .Define("K3_E", GetKE, {"H3_PX", "H3_PY", "H3_PZ"})
                        .Define("B_E", fn_sum, {"K1_E", "K2_E", "K3_E"})
                        .Define("B_m", fn_mass, {"B_E", "B_P2"});
auto hMass = df_mass.Histo1D<double>({"B_mass", "", 500, 5050, 5500}, "B_m");
```

<script>
code = document.getElementsByClassName("sourceCode")[2];
code.style.fontSize = "14px";
code.style.maxHeight = "500px";
code.style.lineHeight = "0";
</script>

### CPU bound, not I/O bound

* input file fits in the filesystem cache
* RNTuple runs at 3.19 GiB/s (raw reads from cache can do 8 on my workstation)

## Status of master

---

![](runtimes_before.svg)

||evts/s|
|--|--|
|RNTuple|21M|
|RNTupleDS|5.8M|

---

<!--
<img src="https://media1.tenor.com/images/2470e5aeeffb9e9bb27145e732827682/tenor.gif?itemid=5693923" width=600px>
-->
![](https://media1.tenor.com/images/2470e5aeeffb9e9bb27145e732827682/tenor.gif?itemid=5693923)

---

#### Before

<object data="rdf_and_rntuple_before.svg" type="image/svg+xml" style="max-inline-size:80%; background-color:whitesmoke;"></object>

<span style="font-size:0.6em">RNTuple has to load all potentially required columns upfront.</span>

---

#### Before

<object data="rdf_and_rntuple_before.svg" type="image/svg+xml" style="max-inline-size:80%; background-color:whitesmoke;"></object>

<span style="font-size:0.6em">RNTuple has to load all potentially required columns upfront.</span>

#### After

<object data="rdf_and_rntuple_after.svg" type="image/svg+xml" style="max-inline-size:80%; background-color:whitesmoke;"></object>

<span style="font-size:0.6em">RNTuple can load values lazily, if and when RDF needs them for a computation.</span>

## Status with lazy RNTupleDS column readers

---

![](runtimes_after.svg)

---

||evts/s|
|--|--|
|RNTuple|21M|
|RNTupleDS after|9.3M|
|RNTupleDS before|5.8M|

### Comparing flamegraphs 1/2

<object data="rntuple.svg" type="image/svg+xml" style="max-inline-size:80%"></object>

---

### Comparing flamegraphs 2/2

<object data="rntupleds_after.svg" type="image/svg+xml" style="max-inline-size:80%"></object>

---

### Comparing stats

||RNTuple|RNTupleDS (after)|
|--|--|--|
|Runtime (s)|2.03|4.62|
|Time in PopulatePage+LoadCluster|88.2% (1.79s)|49.5% (2.28s)|
|CPU instructions|4.9B (0.39 ins/cycle)|20B (1.25 ins/cycle)|
|Branches|1.1B|5.8B|
|Branch misses|19M (1.66%)|24.5M (0.42%)|
|Cache misses|109M|121M|
|iTLB-load-misses|94k|347k|

<script>
document.getElementById("comparing-stats").getElementsByTagName("table")[0].style.fontSize = "32px"
</script>

## Reality check: compressed files

---

Same data, same conditions (warm cache, etc.).

Reporting runtime (s) • rate (MiB/s)

|Compression|RNTuple|RNTupleDS|
|--|--|--|
|Uncompressed|2.03 s|4.62 s|
||3260 MiB/s|1430 MiB/s|
|Zstd|10.0 s|12.7 s|
||662.4 MiB/s|521 MiB/s|
|LZMA|223 s|225 s|
||29.7 MiB/s|29.4 MiB/s|

<small>
No error bars sry
</small>

## Summary

---

* Reduced RNTupleDS overhead (by ~40% for this extreme benchmark)
  * all RDataSources can take advatange of the lazy column reader feature

---

* RDF graph traversal is the biggest overhead w.r.t. raw RNTuple usage
  * internally, RDF could process a bulk of events at each traversal (w.i.p.)

---

* still, ease of use and parallelization make RDF+RNTupleDS an absolutely valid option, especially for compressed files

---

* coming soon: multi-thread RDF+RNTuple without redundant decompressions and related benchmarks
