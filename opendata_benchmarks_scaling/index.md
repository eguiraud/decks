---
title: "A performance study of the RDF opendata benchmarks"
author: Enrico Guiraud
date: May 6 2021, 102nd PPP meeting
---

### The problem

<img src=reported_scaling.png>

<link rel="stylesheet" href="styles.css">

Initial report (+ a looong discussion) [here](https://root-forum.cern.ch/t/scalability-of-rdataframes-on-16-cores/44222).<br>
[RDF benchmarks](https://github.com/root-project/opendata-benchmarks) && [collection of other solutions](https://github.com/iris-hep/adl-benchmarks-index).

---

### Breaking it down

Is it ROOT I/O or RDF?<br>Let's disentangle with [`root-readspeed`](https://github.com/eguiraud/root-readspeed/).

|Pool size|Throughput (MB/s) |%CPU  |Speed-up|
|---------|----------|------|--------|
|0        | 93.255   |99    |  1.0  |
|2        | 185.494  |194   |  2.0  |
|8        | 675.031  |716   |  7.2  |
|16       | 1181.63  |1310  | 12.7  |
|48       | 1113.97  |1673  | 12.0  |
|64       | 1069.62  |1810  | 11.5  |

Terrible scaling with *low CPU usage*.

---

- issue best visible above 16 threads: luckily we now have a beefy machine!
- standard `perf` usage or flamegraphs won't show what's wrong: time is wasted off-CPU
- `gdb` "manual sampling" goes a long way when %CPU is particularly low (quicker/simpler than off-cpu analysis with `perf` or vtune)

---

[Poor man's profiling](https://poormansprofiler.org) using gdb:

<div style="font-size:0.4em">
```
(gdb) info thread
...
  19   Thread 0x7fffd9ff7700 (LWP 107466) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  20   Thread 0x7fffd97f5700 (LWP 107467) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  21   Thread 0x7fffb9ff7700 (LWP 107469) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  22   Thread 0x7fffb93f4700 (LWP 107470) "root-readspeed" 0x00007ffff47b12fc in pthread_cond_wait@@GLIBC_2.3.2 () from /lib64/libpthread.so.0
  23   Thread 0x7fffb97f5700 (LWP 107475) "root-readspeed" 0x00007ffff47b1e12 in pthread_cond_broadcast@@GLIBC_2.3.2 () from /lib64/libpthread.so.0
  24   Thread 0x7fffb8bf2700 (LWP 107478) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  25   Thread 0x7fffba3f8700 (LWP 107480) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  26   Thread 0x7fffb9bf6700 (LWP 107481) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  27   Thread 0x7fffbb3fc700 (LWP 107474) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  28   Thread 0x7fffb8ff3700 (LWP 107476) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  29   Thread 0x7fffba7f9700 (LWP 107479) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  30   Thread 0x7fffbbbfe700 (LWP 107477) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  31   Thread 0x7fffbaffb700 (LWP 107472) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  32   Thread 0x7fffbb7fd700 (LWP 107473) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  33   Thread 0x7fffbabfa700 (LWP 107471) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  34   Thread 0x7fff767f9700 (LWP 107482) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  35   Thread 0x7fff75ff7700 (LWP 107484) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
  36   Thread 0x7fff74bf2700 (LWP 107486) "root-readspeed" 0x00007ffff47b465d in __lll_lock_wait () from /lib64/libpthread.so.0
...
```
</div>

---

And looking at the callstacks (1/2):


<div style="font-size:12pt">
```
(gdb) bt 15
#0  0x00007ffff47a065d in __lll_lock_wait () from /lib64/libpthread.so.0
#1  0x00007ffff4799979 in pthread_mutex_lock () from /lib64/libpthread.so.0
#2  0x00007ffff5130df1 in __gthread_mutex_lock (__mutex=<optimized out>) at /usr/include/c++/8/x86_64-redhat-linux/bits/gthr-default.h:748
#3  std::mutex::lock (this=<optimized out>) at /usr/include/c++/8/bits/std_mutex.h:103
#4  std::unique_lock<std::mutex>::lock (this=0x7fffb7ff9500, this=0x7fffb7ff9500) at /usr/include/c++/8/bits/std_mutex.h:267
#5  std::_V2::condition_variable_any::_Unlock<std::unique_lock<std::mutex> >::~_Unlock (this=<synthetic pointer>, __in_chrg=<optimized out>) at /usr/include/c++/8/condition_variable:223
#6  std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex> > (__lock=..., this=0x1a3fac0) at /usr/include/c++/8/condition_variable:259
#7  std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex>, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::WriteLock()::{lambda()#1}>(std::unique_lock<std::mutex>&, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::WriteLock()::{lambda()#1}) (__p=..., __lock=..., this=0x1a3fac0) at /usr/include/c++/8/condition_variable:272
#8  ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::WriteLock (this=0x1a3fa88) at /data/ssdext4/rdf_scaling/root_src/core/thread/src/TReentrantRWLock.cxx:190
#9  0x00007ffff5499c7b in ROOT::TVirtualRWMutex::Lock (this=0x1a3fa80) at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TVirtualRWMutex.h:79
#10 TLockGuard::TLockGuard (mutex=0x1a3fa80, this=<synthetic pointer>) at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TVirtualMutex.h:77
#11 TFile::~TFile (this=0x7fff880013a0, __in_chrg=<optimized out>) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TFile.cxx:533
#12 0x00007ffff5499de1 in TFile::~TFile (this=0x7fff880013a0, __in_chrg=<optimized out>) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TFile.cxx:507
#13 0x0000000000403cce in std::default_delete<TFile>::operator() (this=<synthetic pointer>, __ptr=0x7fff880013a0) at /usr/include/c++/8/bits/unique_ptr.h:342
#14 std::unique_ptr<TFile, std::default_delete<TFile> >::~unique_ptr (this=<synthetic pointer>, __in_chrg=<optimized out>) at /usr/include/c++/8/bits/unique_ptr.h:269
```
</div>

---

And looking at the callstacks (2/2):

<div style="font-size:11pt">
```
(gdb) bt 15
#0  0x00007ffff47a065d in __lll_lock_wait () from /lib64/libpthread.so.0
#1  0x00007ffff4799979 in pthread_mutex_lock () from /lib64/libpthread.so.0
#2  0x00007ffff5131fb1 in __gthread_mutex_lock (__mutex=<optimized out>) at /usr/include/c++/8/x86_64-redhat-linux/bits/gthr-default.h:748
#3  std::mutex::lock (this=<optimized out>) at /usr/include/c++/8/bits/std_mutex.h:103
#4  std::unique_lock<std::mutex>::lock (this=0x7fff5d7ef0a0, this=0x7fff5d7ef0a0) at /usr/include/c++/8/bits/std_mutex.h:267
#5  std::_V2::condition_variable_any::_Unlock<std::unique_lock<std::mutex> >::~_Unlock (this=<synthetic pointer>, __in_chrg=<optimized out>) at /usr/include/c++/8/condition_variable:223
#6  std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex> > (__lock=..., this=<optimized out>) at /usr/include/c++/8/condition_variable:259
#7  std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex>, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock()::{lambda()#1}>(std::unique_lock<std::mutex>&, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock()::{lambda()#1}) (__p=..., __lock=..., this=<optimized out>) at /usr/include/c++/8/condition_variable:272
#8  ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock (this=0x1a3fa88) at /data/ssdext4/rdf_scaling/root_src/core/thread/src/TReentrantRWLock.cxx:110
#9  0x00007ffff4b96e71 in ROOT::TReadLockGuard::TReadLockGuard (mutex=0x1a3fa80, this=<synthetic pointer>) at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TVirtualRWMutex.h:120
#10 TPluginManager::LoadHandlersFromPluginDirs (this=0x62f7d0, base=<optimized out>) at /data/ssdext4/rdf_scaling/root_src/core/base/src/TPluginManager.cxx:455
#11 0x00007ffff4b98560 in TPluginManager::FindHandler (this=0x62f7d0, base=base@entry=0x7ffff56a0aac "TFile", uri=0x7ffebc2c9cc0 "file:data/Run2012B_SingleMu.root")
    at /data/ssdext4/rdf_scaling/root_src/core/base/src/TPluginManager.cxx:582
#12 0x00007ffff549cef4 in TFile::Open (url=<optimized out>, options=options@entry=0x407225 "", ftitle=ftitle@entry=0x407225 "", compress=compress@entry=101, netopt=netopt@entry=0)
    at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TString.h:244
#13 0x0000000000403ade in ReadTree (treeName="Events", fileName="data/Run2012B_SingleMu.root", branchNames=std::vector of length 7, capacity 8 = {...}, range=...)
    at /usr/include/c++/8/bits/basic_string.h:2290
#14 0x0000000000404905 in <lambda(int)>::<lambda(const EntryRange&)>::operator() (range=..., __closure=0x7fffe48c6760) at /data/ssdext4/rdf_scaling/root-readspeed/src/root_readspeed.cxx:204
```
</div>

<div style="font-size:18pt">
`root-readspeed` locks when opening/closing `TFile`s (including reading streamer infos out of them).
</div>



---

### Teach root-readspeed to re-use files if possible

<style>
code span.va {
   color: green;
</style>

```patch
-   auto f = std::unique_ptr<TFile>(TFile::Open(fname.c_str()));
+   thread_local TFile *f;
+   if (f == nullptr || f->GetName() != fname) {
+     delete f;
+     f = TFile::Open(fname.c_str());
+   }
```

Can't use `unique_ptr` due to teardown sequencing issues.

---

How are we doing now?

|Pool size|Throughput (MB/s) |%CPU  |Speed-up|
|---------|----------|------|--------|
|0        |94.1047   |99    | 1.0   |
|2        |182.486   |195   | 1.9   |
|8        |676.343   |713   | 7.2   |
|16       |1210.47   |1309  | 12.9  |
|48       |2375.57   |2858  | 25.2 (+13) |
|64       |2516.84   |3336  | 26.8 (+15) |

---

### What's limiting the CPU now?

<div style="font-size:0.8em">
- the TFile opening contention is still present at start-up (re-use does not help on first use)
- benchmark runtimes are relatively short (~1s with 48/64 cores)
</div>

<br>

Is this a small-dataset effect?

---

Throughput vs dataset size

|#files |  Throughput@64 threads(MB/s) | %CPU@64 threads |
|---------|----------|------|
|1      |  2495.52           |  3309 |
|2      |  2880.08           |  4259 |
|5      |  3603.36           |  5190 |
|10     |  3984.13           |  5594 |
|20     |  4210.87           |  5829 |
|40     |  4389.58           |  5971 |
|60     |  4455.99           |  6021 |
|80     |  4423.99           |  6012 |
|90     |  4467.27           |  6086 |

Things look ok reading the input file 20+ times.

---

- with a 90x bigger input dataset, we get a 46x speed-up with 64 threads w.r.t. the single-thread version
- that's pretty good according to Amdahl's law ("99.4% of the workload perfectly parallelized")
- so indeed scaling suffers more with smaller datasets/shorter runtimes

---

### Back to RDF

Let's take one of the less simplistic opendata benchmarks (#8) and see how we are doing there with 64 cores.

|Setup|Wall time (s)|%CPU|Speed-up w.r.t 1 core|
|---------|----------|------|------|
|jitting| 8.01 | 2668 | 14.3 |
|compiled| 4.43 | 5413 | 7.3 |
|jitting, 20x data| 70.08 | 5878 | 33.0 |
|compiled, 20x data| 62.87 | 6227 | 10.1 |

---

- on the original dataset with the original macro, low %CPU usage suggests we are limited by the same scaling issue we see in `root-readspeed`
- in other cases %CPU is high but scaling is still bad: this is a different problem!

---

#### Now for some on-cpu performance analysis

<div style="font-size:0.8em">

`perf` is my go-to for that.

<div style="font-size:0.8em">
```
$ perf stat ./benchmark8_compiled 64 20

 Performance counter stats for './benchmark8_compiled 64 20':

      3,919,896.26 msec task-clock:u              #   62.492 CPUs utilized
                 0      context-switches:u        #    0.000 K/sec
                 0      cpu-migrations:u          #    0.000 K/sec
           293,281      page-faults:u             #    0.075 K/sec
12,226,969,413,508      cycles:u                  #    3.119 GHz                      (83.33%)
 3,323,378,376,905      stalled-cycles-frontend:u #   27.18% frontend cycles idle     (83.33%)
 7,052,605,644,349      stalled-cycles-backend:u  #   57.68% backend cycles idle      (83.33%)
 4,670,737,715,683      instructions:u            #    0.38  insn per cycle
                                                  #    1.51  stalled cycles per insn  (83.33%)
   789,662,153,996      branches:u                #  201.450 M/sec                    (83.33%)
    13,175,910,948      branch-misses:u           #    1.67% of all branches          (83.33%)

      62.726124943 seconds time elapsed

    3870.516569000 seconds user
      12.871691000 seconds sys
```
</div>

High `stalled-cycles-backend` count, low instructions/cycle.<br>

</div>

---

### Ok, but _where_? (1/2)

<div style="font-size:0.64em">
```
$ perf record -e stalled-cycles-backend:u -F99 ./benchmark8_compiled 64 20
$ perf report --stdio

# Overhead  Command          Shared Object        Symbol                                                                                                                                       >
# ........  ...............  ...................  ...............
#
  53.85%    benchmark8       benchmark8           RDF::RFilter<...>::CheckFilters
  20.14%    benchmark8       benchmark8           RDF::RFilter<...>::CheckFilters
  2.04%     benchmark8       libTree.so           TTree::LoadTree
```
</div>

<div style="font-size:0.8em">
[Flamegraph for the curious](./benchmark8_compiled_64threads_20xdata.svg).
</div>

---

#### Ok, but _where_?? (2/2)

<img src=perf_annotate.png>

<div style="font-size:0.8em">
`perf` annotations are fairly imprecise in attributing event hits to assembly instructions,
but they are still useful to get an idea of what the critical region is.
Can use `perf c2c` on newer kernels.
</div>

---

Luckily here the problem is fairly obvious (especially if Guilherme told you to fix it already a few times):

```cpp
fLastResult[slot] = passed;
fLastCheckedEntry[slot] = entry;
return fLastResult[slot];
```

Bad scaling due to CPUs being backend-stalled?<br>
It's false sharing.

---

### A trivial fix for false sharing

```patch
- if (entry != fLastCheckedEntry[slot]) {
+ if (entry != fLastCheckedEntry[slot * RDFInternal::CacheLineStep<Long64_t>()]) {
     if (!fPrevData.CheckFilters(slot, entry)) {
        // a filter upstream returned false, cache the result
-       fLastResult[slot] = false;
+       fLastResult[slot * RDFInternal::CacheLineStep<int>()] = false;
```

Basically a DIY `alignas`, as it's tricky to set the alignment of `std::vector` elements.

---

### With the false sharing fix

|Setup|Wall time (s)|%CPU|Speed-up w.r.t 1 core|
|---------|----------|------|------|
|jitting| 7.48 (-0.5) | 2355 (-300) | 15.9 (+1.6) |
|compiled| 1.57 (-2.8) | 3891 (-1500) | 20.4 (+13) |
|jitting, 20x data| 61.09 (-9) | 5835 | 38.11 (+7) |
|compiled, 20x data| 18.24 (-45) | 6031 (-200) | 34.6 (+24) |

[Effect on our benchmarks](https://rootbnch-grafana-test.cern.ch/d/G2qrd2SWk/rdataframe?orgId=1).

---

### The original report, updated

<img src="reported_scaling_with_falsesharing_fix.png">

---

### Summary

- we are reminded that 48/64 threads reveal contention issues that are not visible at lower thread counts
   - it's great to finally have a machine where we can reproduce these issues!
- poor man's profiling can be a boon for off-cpu performance analysis
- tricky (impossible?) to infer scaling behavior of ROOT I/O at high core counts from short-lived benchmarks, we have large constant overheads in that corner
- can we do anything to reduce this overhead?
- diminishing returns: should we be happy with a best speed-up of 35x at 64 threads for RDF, 46x for `root-readspeed`?

---

### More about smaller datasets

---

Poor man's profiler (1/2):

<div style="font-size:0.64em">
```cpp
#0  0x00007ffff479d2fc in pthread_cond_wait@@GLIBC_2.3.2 () from /lib64/libpthread.so.0
#1  0x00007ffff44b68f0 in std::condition_variable::wait(std::unique_lock<std::mutex>&) () from /lib64/libstdc++.so.6
#2  0x00007ffff5131eb2 in std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex> > (__lock=..., this=<optimized out>) at /usr/include/c++/8/bits/std_mutex.h:238
#3  std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex>, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock()::{lambda()#1}>(std::unique_lock<std::mutex>&, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock()::{lambda()#1}) (__p=..., __lock=..., this=<optimized out>) at /usr/include/c++/8/condition_variable:272
#4  ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock (this=0x1a3fd48) at /data/ssdext4/rdf_scaling/root_src/core/thread/src/TReentrantRWLock.cxx:110
#5  0x00007ffff5429c18 in ROOT::TReadLockGuard::TReadLockGuard (mutex=0x1a3fd40, this=<synthetic pointer>) at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TVirtualRWMutex.h:120
#6  TBufferFile::ReadClassBuffer (this=this@entry=0x7fffa27f2c90, cl=0x1f88f10, pointer=pointer@entry=0x7fff683be8f0, version=version@entry=12, start=start@entry=1107062,
    count=count@entry=28099, onFileClass=0x0) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TBufferFile.cxx:3300
#7  0x00007ffff77669d8 in TBranch::Streamer (this=0x7fff683be8f0, b=...) at /data/ssdext4/rdf_scaling/root_src/tree/tree/src/TBranch.cxx:2891
#8  0x00007ffff542b059 in TClass::Streamer (onfile_class=0x0, b=..., obj=0x7fff683be8f0, this=0x1f88f10) at /data/ssdext4/rdf_scaling/root_src/core/meta/inc/TClass.h:609
#9  TBufferFile::ReadObjectAny (this=this@entry=0x7fffa27f2c90, clCast=0x13c1a00) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TBufferFile.cxx:2470
#10 0x00007ffff4c13ef8 in TObjArray::Streamer (this=0x7fff682c90f8, b=...) at /data/ssdext4/rdf_scaling/root_src/core/cont/src/TObjArray.cxx:468
#11 0x00007ffff5672f86 in TStreamerInfo::ReadBuffer<char**> (this=0x1e48820, b=..., arr=@0x7fffa27f2aa8: 0x7fffa27f2aa0, compinfo=0x26d1748, first=first@entry=0, last=last@entry=1, narr=1,
    eoffset=0, arrayMode=0) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TStreamerInfoReadBuffer.cxx:1304
#12 0x00007ffff54f343c in TStreamerInfoActions::GenericReadAction (buf=..., addr=<optimized out>, config=<optimized out>)
    at /data/ssdext4/rdf_scaling/root_src/io/io/src/TStreamerInfoActions.cxx:194
#13 0x00007ffff5429be6 in TStreamerInfoActions::TConfiguredAction::operator() (this=0x22c67d0, this=0x22c67d0, object=0x7fff682c8fa0, buffer=...)
    at /data/ssdext4/rdf_scaling/root_src/io/io/inc/TStreamerInfoActions.h:123
#14 TBufferFile::ApplySequence (obj=0x7fff682c8fa0, sequence=..., this=0x7fffa27f2c90) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TBufferFile.cxx:3572
#15 TBufferFile::ApplySequence (obj=0x7fff682c8fa0, sequence=..., this=0x7fffa27f2c90) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TBufferFile.cxx:3554
#16 TBufferFile::ReadClassBuffer (this=this@entry=0x7fffa27f2c90, cl=<optimized out>, pointer=pointer@entry=0x7fff682c8fa0, version=version@entry=19, start=start@entry=54,
    count=count@entry=2053814, onFileClass=<optimized out>) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TBufferFile.cxx:3354
#17 0x00007ffff77de990 in TTree::Streamer (this=0x7fff682c8fa0, b=...) at /data/ssdext4/rdf_scaling/root_src/tree/tree/src/TTree.cxx:9416
#18 0x00007ffff54cb1be in TClass::Streamer (onfile_class=0x0, b=..., obj=0x7fff682c8fa0, this=0x1ea2940) at /data/ssdext4/rdf_scaling/root_src/core/meta/inc/TClass.h:609
#19 TKey::ReadObjectAny (this=0x7fff683fd910, expectedClass=<optimized out>) at /data/ssdext4/rdf_scaling/root_src/io/io/src/TKey.cxx:1108
#20 0x00007ffff547a282 in TDirectoryFile::GetObjectChecked (this=this@entry=0x7fff680013a0, namecycle=namecycle@entry=0x6c6730 "Events", expectedClass=0x1ea2940)
    at /data/ssdext4/rdf_scaling/root_src/io/io/src/TDirectoryFile.cxx:1075
#21 0x0000000000403b23 in TDirectory::Get<TTree> (namecycle=<optimized out>, this=0x7fff680013a0) at /data/ssdext4/rdf_scaling/root_src/core/meta/inc/TClass.h:650
#22 TDirectoryFile::Get<TTree> (namecycle=<optimized out>, this=0x7fff680013a0) at /data/ssdext4/rdf_scaling/root_src/io/io/inc/TDirectoryFile.h:83
#23 ReadTree (treeName="Events", fileName="data/Run2012B_SingleMu.root", branchNames=std::vector of length 7, capacity 8 = {...}, range=...)
    at /data/ssdext4/rdf_scaling/root-readspeed/src/root_readspeed.cxx:60
```
</div>

---

Poor man's profiler (1/2):

<div style="font-size:0.64em">
```cpp
#0  0x00007ffff47a065d in __lll_lock_wait () from /lib64/libpthread.so.0
#1  0x00007ffff4799979 in pthread_mutex_lock () from /lib64/libpthread.so.0
#2  0x00007ffff5131fb1 in __gthread_mutex_lock (__mutex=<optimized out>) at /usr/include/c++/8/x86_64-redhat-linux/bits/gthr-default.h:748
#3  std::mutex::lock (this=<optimized out>) at /usr/include/c++/8/bits/std_mutex.h:103
#4  std::unique_lock<std::mutex>::lock (this=0x7fff6eff4f00, this=0x7fff6eff4f00) at /usr/include/c++/8/bits/std_mutex.h:267
#5  std::_V2::condition_variable_any::_Unlock<std::unique_lock<std::mutex> >::~_Unlock (this=<synthetic pointer>, __in_chrg=<optimized out>) at /usr/include/c++/8/condition_variable:223
#6  std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex> > (__lock=..., this=<optimized out>) at /usr/include/c++/8/condition_variable:259
#7  std::_V2::condition_variable_any::wait<std::unique_lock<std::mutex>, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock()::{lambda()#1}>(std::unique_lock<std::mutex>&, ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock()::{lambda()#1}) (__p=..., __lock=..., this=<optimized out>) at /usr/include/c++/8/condition_variable:272
#8  ROOT::TReentrantRWLock<std::mutex, ROOT::Internal::RecurseCounts>::ReadLock (this=0x1a3fd48) at /data/ssdext4/rdf_scaling/root_src/core/thread/src/TReentrantRWLock.cxx:110
#9  0x00007ffff4b96e71 in ROOT::TReadLockGuard::TReadLockGuard (mutex=0x1a3fd40, this=<synthetic pointer>) at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TVirtualRWMutex.h:120
#10 TPluginManager::LoadHandlersFromPluginDirs (this=0x62f7d0, base=<optimized out>) at /data/ssdext4/rdf_scaling/root_src/core/base/src/TPluginManager.cxx:455
#11 0x00007ffff4b98560 in TPluginManager::FindHandler (this=0x62f7d0, base=base@entry=0x7ffff56a0a02 "TArchiveFile", uri=0x7fff08000bc0 "data/Run2012B_SingleMu.root")
    at /data/ssdext4/rdf_scaling/root_src/core/base/src/TPluginManager.cxx:582
#12 0x00007ffff5420f46 in TArchiveFile::Open (url=0x7fff08000b60 "file:data/Run2012B_SingleMu.root", file=file@entry=0x7fff08000fd0) at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TString.h:244
#13 0x00007ffff5498d67 in TFile::TFile (this=0x7fff08000fd0, fname1=0x7fff0833d080 "data/Run2012B_SingleMu.root", option=<optimized out>, ftitle=<optimized out>, compress=<optimized out>)
    at /data/ssdext4/rdf_scaling/root_src/io/io/src/TFile.cxx:355
#14 0x00007ffff549cf5c in TFile::Open (url=<optimized out>, options=options@entry=0x407225 "", ftitle=ftitle@entry=0x407225 "", compress=compress@entry=101, netopt=netopt@entry=0)
    at /data/ssdext4/rdf_scaling/root_src/core/base/inc/TObject.h:167
#15 0x0000000000403ade in ReadTree (treeName="Events", fileName="data/Run2012B_SingleMu.root", branchNames=std::vector of length 7, capacity 8 = {...}, range=...)
    at /usr/include/c++/8/bits/basic_string.h:2290
#16 0x0000000000404905 in <lambda(int)>::<lambda(const EntryRange&)>::operator() (range=..., __closure=0x7fffe48c7760) at /data/ssdext4/rdf_scaling/root-readspeed/src/root_readspeed.cxx:204
#17 ROOT::TThreadExecutor::<lambda(unsigned int)>::operator() (this=<optimized out>, this=<optimized out>, i=<optimized out>)
    at /data/ssdext4/rdf_scaling/root_src/core/imt/inc/ROOT/TThreadExecutor.hxx:231
#18 std::_Function_handler<void(unsigned int), ROOT::TThreadExecutor::Foreach(F, const std::vector<T, std::allocator<_T2> >&, unsigned int) [with F = EvalThroughputMT(const Data&, unsigned int)::<lambda(int)> mutable::<lambda(const EntryRange&)>; T = EntryRange]::<lambda(unsigned int)> >::_M_invoke(const std::_Any_data &, unsigned int &&) (__functor=..., __args#0=<optimized out>)
    at /usr/include/c++/8/bits/std_function.h:297
#19 0x00007ffff5e096b8 in std::function<void (unsigned int)>::operator()(unsigned int) const (__args#0=<optimized out>, this=<optimized out>) at /usr/include/c++/8/bits/std_function.h:682
#20 tbb::detail::d1::parallel_for_body<std::function<void (unsigned int)>, unsigned int>::operator()(tbb::detail::d1::blocked_range<unsigned int> const&) const (r=..., this=0x7fffd8387b50)
    at /data/ssdext4/rdf_scaling/oneTBB/install/include/oneapi/tbb/parallel_for.h:183
#21 tbb::detail::d1::start_for<tbb::detail::d1::blocked_range<unsigned int>, tbb::detail::d1::parallel_for_body<std::function<void (unsigned int)>, unsigned int>, tbb::detail::d1::auto_partitioner const>::run_body(tbb::detail::d1::blocked_range<unsigned int>&) (r=..., this=0x7fffd8387b00) at /data/ssdext4/rdf_scaling/oneTBB/install/include/oneapi/tbb/parallel_for.h:94
#22 tbb::detail::d1::dynamic_grainsize_mode<tbb::detail::d1::adaptive_mode<tbb::detail::d1::auto_partition_type> >::work_balance<tbb::detail::d1::start_for<tbb::detail::d1::blocked_range<unsigned int>, tbb::detail::d1::parallel_for_body<std::function<void (unsigned int)>, unsigned int>, tbb::detail::d1::auto_partitioner const>, tbb::detail::d1::blocked_range<unsigned int> >(tbb::detail::d1::start_for<tbb::detail::d1::blocked_range<unsigned int>, tbb::detail::d1::parallel_for_body<std::function<void (unsigned int)>, unsigned int>, tbb::detail::d1::auto_partitioner const>&, tbb::detail::d1::blocked_range<unsigned int>&, tbb::detail::d1::execution_data&) (ed=..., range=..., start=warning: RTTI symbol not found for class 'tbb::detail::d1::start_for<tbb::detail::d1::blocked_range<unsigned int>, tbb::detail::d1::parallel_for_body<std::function<void (unsigned int)>, unsigned int>, tbb::detail::d1::auto_partitioner const>'
..., this=<optimized out>) at /data/ssdext4/rdf_scaling/oneTBB/install/include/oneapi/tbb/partitioner.h:454
```
</div>
