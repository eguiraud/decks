---
title: "ROOT I/O performance profiling: what and how"
author: Enrico Guiraud
date: April 4 2021, 99th PPP meeting
---

## A ROOT plan of work item

"Implement / review perf metrics for I/O ("TTreePerfStats" for MT + RNTuple), also for standardized user reports"

<link rel="stylesheet" href="styles.css">

---

## Today

Discuss what we have, what we want, how we get there.

Not a "talk": slides are just a support for that discussion.

## What (I think) we want

Tooling to answer the following questions:

<div style="font-size:32px">
- what is my analysis performance bottleneck? Raw I/O vs decompression vs deserialization vs number crunching
- how does this setup compare to others in event processing speed? and in data I/O speed (including decompression+deserialization)
- what is the best ROOT I/O configuration for this analysis? (#threads, compression settings, cache size, async pre-fetch or not, ...)
- how much data am I _actually_ reading?
- what is my disk access pattern?
</div>

## What we have

- [TTreePerfStats](https://root.cern.ch/doc/master/classTTreePerfStats.html)
- [RNTuple::EnableMetrics](https://root.cern.ch/doc/master/ntpl005__introspection_8C.html) (experimental)
- [root-readspeed](https://github.com/eguiraud/root-readspeed/) (experimental)

## TTreePerfStas

---

### Usage example

```cpp
auto *t = file->Get<TTree>("Events");
TTreePerfStats ps("ioperf", t);
for (Int_t i=0; i < nentries; ++i)
   t->GetEntry(i);
ps.Print(); // or ps.GetXXX(), or ps.Draw()
```

---

### Feature overview

- TTree read/write support
- TChain support should have been added last January, but no test exists afaict (see [Jira 1](https://sft.its.cern.ch/jira/browse/ROOT-5079), [Jira 2](https://sft.its.cern.ch/jira/browse/ROOT-10538), [PR](https://github.com/root-project/root/pull/7082)
- are friend TTrees/TChains supported?
- no support for multi-threading
- rich output: among other metrics raw I/O time, number of disk reads, number of compressed and uncompressed bytes are reported

---

## RNTuple metrics

---

### Usage example

```cpp
// enable I/O perf. counters
ntuple->EnableMetrics();
auto viewX = ntuple->GetView<double>("v3.fX");
for (auto i : ntuple->GetEntryRange()) {
   viewX(i);
}
ntuple->PrintInfo(ENTupleInfo::kMetrics);
float nbytesRead = ntuple->GetMetrics().GetCounter("RNTupleReader.RPageSourceFile.szReadPayload")->GetValueAsInt() +
                   ntuple->GetMetrics().GetCounter("RNTupleReader.RPageSourceFile.szReadOverhead")->GetValueAsInt();
```

```cpp
// EXAMPLE OUTPUT
RNTupleReader.RPageSourceFile.nReadV||number of vector read requests|134
RNTupleReader.RPageSourceFile.nRead||number of byte ranges read|802
RNTupleReader.RPageSourceFile.szReadPayload|B|volume read from file (required)|1129407576
RNTupleReader.RPageSourceFile.szReadOverhead|B|volume read from file (overhead)|259587544
RNTupleReader.RPageSourceFile.szUnzip|B|volume after unzipping|1129407576
RNTupleReader.RPageSourceFile.nClusterLoaded||number of partial clusters preloaded from storage|134
RNTupleReader.RPageSourceFile.nPageLoaded||number of pages loaded from storage|16848
RNTupleReader.RPageSourceFile.nPagePopulated||number of populated pages|16848
RNTupleReader.RPageSourceFile.timeWallRead|ns|wall clock time spent reading|993490948
RNTupleReader.RPageSourceFile.timeWallUnzip|ns|wall clock time spent decompressing|126018571
RNTupleReader.RPageSourceFile.timeCpuRead|ns|CPU time spent reading|299473000
RNTupleReader.RPageSourceFile.timeCpuUnzip|ns|CPU time spent decompressing|133119999
RNTupleReader.RPageSourceFile.bwRead|MB/s|bandwidth compressed bytes read per second|1398.095396
RNTupleReader.RPageSourceFile.bwReadUnzip|MB/s|bandwidth uncompressed bytes read per second|1136.807113
RNTupleReader.RPageSourceFile.bwUnzip|MB/s|decompression bandwidth of uncompressed bytes per second|8962.231257
RNTupleReader.RPageSourceFile.rtReadEfficiency||ratio of payload over all bytes read|0.813111
RNTupleReader.RPageSourceFile.rtCompression||ratio of compressed bytes / uncompressed bytes|1.000000
```

---

### Feature overview

- RNTuple read/write support
- support for joined ntuples is foreseen (calling `EnableMetrics` on the main ntuple propagates to the others)
- multi-thread support
- TChain-like support foreseen
- rich set of metrics, in line with TTreePerfStats

---

## root-readspeed

---

### What and why

- a tool to measure the _best_ throughput that can be expected from ROOT for a given combination of hardware, dataset and number of threads, taking user code out of the equation
- tries to help users answering "where is the performance bottleneck?" by providing performance metrics
for the usecase in which ROOT runs "as fast as possible given the setup"
- comes with a [README](https://github.com/eguiraud/root-readspeed/) that should help interpret its output.

---

### Usage example

```bash
$ root-readspeed --trees t --files files*.root --threads 4 --branches x y
Real time to setup MT run:      0.0862889 s
CPU time to setup MT run:       0.09 s
Real time:                      0.172477 s
CPU time:                       0.34 s
Uncompressed data read:         80000000 bytes
Throughput:                     442.343 MB/s
```

---

### Feature overview

- (multiple) TTree read-only support
- no direct friend TTree support
- multi-thread support
- set of reported metrics fairly limited compared to other methods

---

## Uncompressed vs compressed bytes read

Probably obvious to most of you, but they are not equivalent. Not always clear what is reported.

Also: total dataset size vs bytes actually read.

---

### compressed bytes read / wall time

- a measure of raw I/O speed _if that is the bottleneck_
- cannot be used to directly compare throughput of different compression algorithms or different data formats (e.g. TTree vs RNTuple)
- at fixed compression settings and analysis code, can be used to predict runtimes on different dataset sizes

---

### uncompressed bytes read / wall time

- a format-independent measure of event processing speed
- can be used to compare throughput of different compression algorithms or different data formats
- tricky to use to predict runtimes of a given application (requires the compression factor, but it varies across branches...)

---

## Extras

- distributions rather than counters? e.g. for task duration, event size, etc.
- instrumentation to trace execution à la [Paraver](https://tools.bsc.es/paraver)? e.g. to track task start/end
