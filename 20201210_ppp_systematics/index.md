---
title: Systematics - RDF's point of view
---

## Types of systematics

1. changing the input tree (aka the sample)
3. changing the value of a column
2. changing what column is used

---

## 1. Changing input samples

![](changing_input_sample.png)

---

## 1. Changing input samples with friends

![](changing_input_sample_2.png)

---

## 2-3. Changing a column

```python
all_histos = []
for var in variations:
   col_name = "col" + var;
   h = df.Define(col_name, ...).Histo1D(col_name)
   all_histos.insert(h)
```

---

## 2. Changing the value of a column (proposal)

```cpp
// like a Define, but produces N different values, one per variation
auto df = _df.Vary("pt_v",
                   [] (double pt, double var) { return pt*var; },
                   {"pt"},
                   {1.0, 0.9, 1.1});

auto nominal_h = df.Filter([] (double pt) { return pt > K; }, {"pt_v"})
                   .Histo1D("pt_v");

// a list/map/dictionary of histograms, one per variation
auto all_h = ROOT::RDF::VariationsFor(nominal_histo);
```
---

## 3. Changing what column is used (proposal)

```cpp
auto df = _df.Vary("pt_v",
            [] (double pt, double pt2, int var) { return var == 1 ? pt : pt2; },
            {"pt", "sample2.pt"},
            {1,2});

auto nominal_h = df.Filter([] (double pt) { return pt > K; }, {"pt_v"})
                   .Histo1D("pt_v");

auto all_h = ROOT::RDF::VariationsFor(nominal_histo);
```

<link rel="stylesheet" href="styles.css">
