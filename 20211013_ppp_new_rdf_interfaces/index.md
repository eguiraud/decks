---
title: New RDF interfaces
author: Enrico Guiraud
date: PPP meeting, 14/10/2021
---

## This talk

- present the user side of some new features in order to gather early feedback/criticism
- not an exhaustive list, but these have been the most requested

## Redefine

```cpp
df.Define("x", []{ return 42; })
  .Redefine("x",[]{ return ROOT::RVecI{1,2,3}; });
```

```python
df.Define('x', '42').Redefine('x', 'ROOT::RVecI{1,2,3}')
```

Already in master.
Might add a `force` parameter to "redefine" non-existing columns.

---

## DefinePerSample (1/2)

It should greatly reduce the need to build separate computation graphs for each "kind" of input (e.g. MC vs data).

```cpp
auto evalWeight = [](unsigned slot, const RSampleInfo& s) { s.Contains("MC") ? 0.5 : 1.; };
df.DefinePerSample("weight", evalWeight);
```

```python
# only rdfslot_ and rdfsampleinfo_ are available, no column values
df.DefinePerSample('weight',
                   'rdfsampleinfo_.Contains("MC") ? 0.5 : 1.')
```

Called once per TTree or once per multi-thread task, whichever is more often.

---

## DefinePerSample (2/2)

It can also be used to inject callbacks that are only run
when the processing of a new section of the data starts.

```cpp
auto log = [](unsigned, const RSampleInfo& s) { Log(s.AsString(), s.EntryRange()); return 0; };
df.DefinePerSample("tmp", log);
```

Already in master.<br>

## DefinePerSample enables a nice progress bar (almost)

```python
# proposal:
# can be called on any node, returns nothing, idempotent
df.ProgressBar() # turn off with df.ProgressBar(False)
```

Other options:

```python
ROOT.RDF.AddProgressBar(df)
ROOT.RDataFrame(..., progressbar=True)
# ...but we have many constructors (and factory functions)
```

## Letting users specify what qualifies as a sample (proposal)

```python
ds = ROOT.RDF.DataSetSpec()
ds.Add('tree', 'mc/*.root', label='MC')
ds.Add('tree', 'data/*.root', label='data')
df = ROOT.RDataFrame(ds)
df.DefinePerSample('x', 'rdfsampleinfo_.Label() == "MC" ? 0.5 : 1.')
```

It needs to also support friend trees/chains.

## DefinePerSample: hard to numba-jit

Numba-jitted functions can't take RSampleInfo arguments,
users must write C++ strings to use the feature.

```python
# does not work, RSampleInfo is not numba-friendly:
@ROOT.Numba.Declare(...)
def evalWeight(slot, sample_info):
   ...
```

---

## On-the-fly systematic variations

```python
# current "solution":
all_histos = []
for var in variations:
   col_name = 'col' + var;
   h = df.Define(col_name, ...).Histo1D(col_name)
   all_histos.insert(h)
```

Lots of book-keeping to keep track of multiple variations.<br>
Slower than necessary because of the branching in the computation graph.

---

## On-the-fly systematic variations (proposal)

```cpp
auto df = _df.Vary("pt",
                   [](float pt) { return RVecF{pt*0.9, pt*1.1}; },
                   /*inputColumns=*/{"pt"},
                   /*variationTags=*/{"down", "up"});

auto nominal_h = df.Histo1D("pt");

// returns a "binder" of all output histograms, one per variation
// all_h["nominal"], all_h["pt:down"], all_h["pt:up"] are available
auto all_h = ROOT::RDF::VariationsFor(nominal_h);
```

- the varied column must already exist
- `Vary` is used to specify "varied" values for it

## Giving variations a name

```cpp
auto df = _df.Vary("pt",
                   [](float pt, float a, float b) { return RVecF{pt*0.9*a, pt*1.1*b}; },
                   /*inputColumns=*/{"pt", "a", "b"},
                   /*variationTags=*/{"down", "up"},
                   /*variationName=*/"ptvariation");

auto nominal_h = df.Histo1D("pt");
auto all_h = ROOT::RDF::VariationsFor(nominal_h);
// valid keys for all_h are {"nominal", "ptvariation:down", "ptvariation:up"}
```

- by default, `variationName` is equal to the name of the varied column.
- note that we can take any columns as input for the expression.

## Multiple systematic variations

```cpp
auto df = _df.Vary("pt",
                   "RVecD{pt*0.9, pt*1.1}",
                   {"down", "up"})
             .Vary("eta",
                   [](float eta) { return RVecF{eta*0.9, eta*1.1}; },
                   {"eta"},
                   /*nVariations=*/2);
auto nom_h = df.Histo2D("pt", "eta");
auto all_h = ROOT::RDF::VariationsFor(nom_h);
// all_h has keys {"nominal", "pt:down", "pt:up", "eta:0", "eta:1"}
```

Note the jitted version and the option to specify a _number_ of
variations instead of a list of names for them (saves typing).

## Using different columns for different variations

```cpp
df.Vary("pt",
        [](float ptdown, float ptup) { return RVecF{ptdown, ptup}; },
        {"frienddown.pt", "friendup.pt"},
        {"down", "up"});
```

## Simultaneous variation of columns

```python
df.Vary({"pt", "eta"},
        "RVec<RVecF>{{pt*0.9, pt*1.1}, {eta*0.9, eta*1.1}}",
        {"down", "up"},
        "ptAndEta")
```

- will produce 3 "universes": `{"nominal", "ptAndEta:down", "ptAndEta:up"}`
- `"pt"` and `"eta"` will vary in lockstep rather than one at a time
- note that we return an RVec of RVecs: each inner RVec contains the variations for one variable

## Multiple variations for the same column

```python
h = df.Vary({"pt", "eta"},
            "RVec<RVecF>{{pt*0.9, pt*1.1}, {eta*0.9, eta*1.1}}",
            {"down", "up"},
            "ptAndEta")
      .Vary("pt", "RVecF{pt+0.1, pt-0.1}", 2, "ptshift")
      .Histo1D("pt")
all_h = ROOT.RDF.VariationsFor(h)
# all_h has keys ["nominal", ptAndEta:down", ptAndEta:up", "ptshift:0", "ptshift:1"]
```

It's fairly natural to have a column vary in lockstep with another in certain "universes",
and on its own in others.<br>
Could not think of a case that breaks (so far).

## Vary, Filter, Define

```python
nominal_h = df.Vary("pt", "RVecD{pt*0.9, pt*1.1}", 2)\
              .Filter("pt > k")\
              .Define("x", "someFunc(pt)")\
              .Histo1D("x")
all_h = ROOT.RDF.VariationsFor(nominal_h)
# all_h has keys ["nominal", "pt:0", "pt:1"]
```

Filters and Defines that depend on varied columns will internally be invoked once per variation.<br>
Users don't have to worry about it.

## Variations and collections

```python
df.Vary("ptvec", "RVec<RVecF>{ptvec*0.9, ptvec*1.1}", 2)\
  .Redefine("ptvec", "ptvec[some_cut]")\
  .Histo1D("ptvec")
```

As usual, collections are not special: they behave the same as scalar values.

## Getting rid of C++ strings in Python (proposal) (1/5)

Current usage of numba-jitted functions:

```python
@ROOT.Numba.Declare(['int', 'int'], 'int')
def foo(x, y):
   return x + y

df.Define("z", "Numba::foo(x, y)")
```

## Getting rid of C++ strings in Python (2/5)

Usage within RDF could be simplified:

```python
def foo(x, y):
   return x + y

# automatically calls Numba.Declare with right types.
# input column names are inferred from function argument names.
df.Define("z", foo)
df.Define("z", lambda x, y: x + y)
```

## Getting rid of C++ strings in Python (3/5)

If helper function comes from C++:

```python
df.Define("x", "myFunction(pt, eta, phi)")
```

a pythonization could allow:

```python
df.Define("x", ROOT.myFunction, ["pt", "eta", "phi"])
```

See also [Josh's PPP talk](https://indico.cern.ch/e/PPP115) from last week.

---

## Getting rid of C++ strings in Python (4/5)

We could infer the input column names from the fuction argument names.
We saw it before for Python, we can do the same in C++ thanks to cling:

```cpp
int myFunction(float pt, float eta, float phi);
```

```python
df.Define("x", ROOT.myFunction)
```

---

## Simultaneous operations on arrays (1/3)

The problem:

```python
df.Define("good_hits", "...")\
  .Define("good_hit_pos", hit_position[good_hits])\
  .Define("good_hit_t", hit_time[good_hits])\
  .Define("good_hit_e", hit_energy[good_hits])\
  .Define("good_hit_pdg", hit_pdg[good_hits])\
```

See also [this forum thread](https://root-forum.cern.ch/t/is-there-better-way-to-filter-array-branches-than-defining-new-columns-in-rdataframe/45495).

## Simultaneous operations on arrays (proposal, 2/3)

```cpp
// note the template parameter
df.Select<RVecF>({"muon_pt", "muon_eta", "muon_phi"},
                 [](RVecF &muon_eta) { return muon_eta > k; },
                 {"muon_eta"});
```

```python
df.Select(["muon_pt", "muon_eta", "muon_phi"], "muon_eta > 0")
```

- compact, mask is evaluated only once
- solves selection, not sorting or other operations
- no natural way to infer type of collections at compile-time: template argument to `Select` is required to avoid jitting

## Simultaneous operations on arrays (proposal, 3/3)

```cpp
df.RedefineColl({"muon_pt", "muon_eta", "muon_phi"},
                [](RVecF &coll, RVecF &muon_eta) { return coll[muon_eta > 0]; },
                /*otherInputCols=*/{"muon_eta"});
```

- more general
- can infer input collection type from callable signature
- if used e.g. for masking, mask would be evaluated once per collection

## EOF

<link rel="stylesheet" href="styles.css">
